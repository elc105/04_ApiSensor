<?php // Clase que nos devuelve la conexion con el proveedor que se desee

class Conexion {
 
 public static function ObtenerConexion()
 {
    try
    {       
         //$Conexion = new mysqli("localhost", "root", "123456", "sensor"); //local 
         $Conexion = new mysqli("us-cdbr-east-06.cleardb.net", "b9e2e3556fd675", "92e6b012", "heroku_bffcf28b91223aa"); // remote
         if (mysqli_connect_errno()){       
            die("No se puede conectar a la base de datos:");
        }
        else 
        {
           return($Conexion);
        }         
    }
    catch (Exception $ex)
    { 
       echo $ex;     
    }
 }

}

?>
