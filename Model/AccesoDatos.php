<?php // Model: Data Access Layer - Capa Acceso Datos

require_once 'Conexion.php';
require_once 'Controller/Temperatura.php';


class AccesoDatos
{
    
  private $cn = NULL;      // Alias para la Conexion
  private $vecr = array(); // Vector con Resultados
  
  public function BuscarRegistro($DatoBuscar)
  { // Funcion para buscar un registro especifico             
      $cn = Conexion::ObtenerConexion();
      $Lista = array();
     try 
     {           
      $rs= $cn->query("SELECT * FROM temperatura WHERE ClienteID='".$DatoBuscar."'");  

      $i=0;
        while ($fila = $rs->fetch_assoc()) 
        {
          $temp = new Temperatura();
          $temp->setID($fila['id']);
          $temp->setTemperatura($fila['Temperatura']);
          $temp->setHumedad($fila['humedad']); 
          $temp->setFechaRegistro($fila['FechaRegistro']); 
          $temp->setClienteID($fila['ClienteID']); 
          $temp->setFechaCreacion($fila['_CreatedAt']); 
          array_push($Lista, $temp);
        }
        mysqli_free_result($rs);
        mysqli_close($cn);
        return $Lista;
     }
     catch (Exception $ex)
     { 
       mysqli_close($cn);
       echo $ex;     
     }
  }


  public function obtenerConectados()
  { // Funcion para buscar los sensores conectados
      $cn = Conexion::ObtenerConexion();
      $Lista = array();
      try 
      {           
        $rs= $cn->query("SELECT * FROM sensores ");  

        while ($fila = $rs->fetch_assoc()) 
        {
          array_push($Lista, [
            "id"=> $fila["Dispositivo"],
            "temp"=> $fila["UltTemp"],
            "hum"=> $fila["UltHum"],
            "estado"=> $fila["estado"]=="C"?true:false,
            "ultimoRegistro"=> $fila["UltConexion"]
          ]);
        }
        mysqli_free_result($rs);
        mysqli_close($cn);
        return $Lista;
     }
     catch (Exception $ex)
     { 
       mysqli_close($cn);
       echo $ex;     
     }
  }
   
  public function ObtenerListadoTemperaturaSensorSemana($id)
  {
    $cn = Conexion::ObtenerConexion();
    $Lista = array();
      try 
      {   
        $rs= $cn->query("SELECT * FROM temperatura WHERE ClienteID='".$id."' AND WEEKOFYEAR(FechaRegistro)=WEEKOFYEAR(NOW()); ");        

        while ($fila = $rs->fetch_assoc()) 
        {
          array_push($Lista, [
            "temp"=> $fila["Temperatura"],
            "time"=> $fila["FechaRegistro"],
            "hum"=> $fila["humedad"],
            "idDispositivo"=> $fila["ClienteID"],
          ]);
        }
        mysqli_free_result($rs);
        mysqli_close($cn);
        return $Lista;
     }
     catch (Exception $ex)
     { 
       mysqli_close($cn);
       echo $ex;     
     }
  }

  public function ObtenerListadoTemperaturaSensorDia($id)
  {
    $cn = Conexion::ObtenerConexion();
    $Lista = array();
      try 
      {   
        $rs= $cn->query("SELECT * FROM temperatura WHERE ClienteID='".$id."' AND YEAR(FechaRegistro) = YEAR(NOW()) AND MONTH(FechaRegistro) = MONTH(NOW()) AND DAY(FechaRegistro) = DAY(NOW());");        

        while ($fila = $rs->fetch_assoc()) 
        {
          array_push($Lista, [
            "temp"=> $fila["Temperatura"],
            "time"=> $fila["FechaRegistro"],
            "hum"=> $fila["humedad"],
            "idDispositivo"=> $fila["ClienteID"],
          ]);
        }
        mysqli_free_result($rs);
        mysqli_close($cn);
        return $Lista;
     }
     catch (Exception $ex)
     { 
       mysqli_close($cn);
       echo $ex;     
     }
  }

  public function ObtenerListadoTemperaturaSensorMes($id)
  {
    $cn = Conexion::ObtenerConexion();
    $Lista = array();
      try 
      {   
        $rs= $cn->query("SELECT * FROM temperatura WHERE ClienteID='".$id."' AND YEAR(FechaRegistro) = YEAR(NOW()) AND MONTH(FechaRegistro)=MONTH(NOW());");        

        while ($fila = $rs->fetch_assoc()) 
        {
          array_push($Lista, [
            "temp"=> $fila["Temperatura"],
            "time"=> $fila["FechaRegistro"],
            "hum"=> $fila["humedad"],
            "idDispositivo"=> $fila["ClienteID"],
          ]);
        }
        mysqli_free_result($rs);
        mysqli_close($cn);
        return $Lista;
     }
     catch (Exception $ex)
     { 
       mysqli_close($cn);
       echo $ex;     
     }
  }
  public function ObtenerListadoTemperatura() 
  {
    $cn = Conexion::ObtenerConexion();
    $DatoBuscar = 0;
    $Lista = array();
    $vecr = array(); 
    try
       {
            $rs= $cn->query("CALL spr_Listados('" . $DatoBuscar . "')");  
            $i=0;
            while ($fila = $rs->fetch_row()) {
                   array_push($vecr, $fila);   
                   $temp = new Temperatura();
                   $temp->setID($vecr[$i][0]);
                   $temp->setTemperatura($vecr[$i][1]);
                   $temp->setHumedad($vecr[$i][2]); 
                   $temp->setFechaRegistro($vecr[$i][3]); 
                   $temp->setClienteID($vecr[$i][4]); 
                   $temp->setFechaCreacion($vecr[$i][5]); 
                   array_push($Lista, $temp);
                   $i++;
            }
            mysqli_free_result($rs);
            mysqli_close($cn);
            return $Lista;
      }
      catch (Exception $ex)
      {
          echo $ex;
      }   
 }
}

?>


