<?php // Esta es una clase transversal, desde donde se instancia el Factory Method
     require_once 'Api/Api.php';
     require_once 'Api/Factory.php';
     
class Funciones
{   
   
   public static function CrearControlador()
   {
        $accesodatos = new AccesoDatos();
        $accesodatos = Factory::GetAccesoDatos($accesodatos);       
        return new ControladorApi($accesodatos);
    }
}