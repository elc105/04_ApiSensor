<?php // Este es el Controlador Manager general de todo el sistema. Todo tiene que pasar por esta clase
     require_once 'Model/AccesoDatos.php';       
     
 class ControladorApi {
    
    protected $accesoDatos;
    
    public function __construct(Accesodatos $iaccesoDatos)
    {
        $this->accesoDatos=new AccesoDatos();
    }             
  
    public function ObtenerListadoTemperaturas() 
    {
       return $this->accesoDatos->ObtenerListadoTemperatura();
    }

    public function ObtenerListadoTemperaturaSensor($value) 
    {
       return $this->accesoDatos->BuscarRegistro($value);
    }

    public function ObtenerListadoConectados()
    {
      return $this->accesoDatos->obtenerConectados();
    }
    public function ObtenerListadoTemperaturaSensorSemana($value)
    {
      return $this->accesoDatos->ObtenerListadoTemperaturaSensorSemana($value);
    }
    public function ObtenerListadoTemperaturaSensorDia($value)
    {
      return $this->accesoDatos->ObtenerListadoTemperaturaSensorDia($value);
    }
    public function ObtenerListadoTemperaturaSensorMes($value)
    {
      return $this->accesoDatos->ObtenerListadoTemperaturaSensorMes($value);
    }
 }
