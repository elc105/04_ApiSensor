<?php // Este es el servicio Rest como tal, quien recibe las peticiones desde el exterior
     require_once 'Funciones.php';       
     
     
class TokenAPI {
    
    public function __construct(){}
             
    public function API(){
        header('Content-Type: application/JSON');                
        $method = $_SERVER['REQUEST_METHOD'];
        switch ($method) {
        case 'GET':
             $this->ObtenerTemperatura();
             break;     
        case 'POST':
            // TO-DO
             break;                
        case 'PUT':
             // TO-DO
             break;                         
        case 'DELETE':
             // TO-DO
             break;
        default: 
                echo 'Metodo No Valido';
                break;
        }
    }
    
    
 function ObtenerTemperatura()
 {
    
   if(isset($_GET['action']))
   {  
      $DatoBuscar=NULL;
      if(isset($_GET['id']))
         $DatoBuscar = $_GET['id'];
      
      $controlador = Funciones::CrearControlador(); 
      
      // Devuelve la lsita de todas las temperaturas registradas
      if ($_GET['action']=='Ltemperatura') 
      {
         $response = $controlador->ObtenerListadoTemperaturas();              
         echo json_encode($response, JSON_PRETTY_PRINT);
      }     
      
      // Devuelve la lista de todas las temperaturas de un sensor dado
      if ($_GET['action']=='sensordata')
      {
         $response = $controlador->ObtenerListadoTemperaturaSensor($DatoBuscar);             
         echo json_encode($response, JSON_PRETTY_PRINT);
      }         

      if ($_GET['action']=='getDispositivos')
      {
         $response = $controlador->ObtenerListadoConectados();             
         echo json_encode($response, JSON_PRETTY_PRINT);
      }
      
      if ($_GET['action']=='getHistorialPorSemana')
      {
         $response = $controlador->ObtenerListadoTemperaturaSensorSemana($DatoBuscar);             
         echo json_encode($response, JSON_PRETTY_PRINT);
      }

      if ($_GET['action']=='getHistorialPorDia')
      {
         $response = $controlador->ObtenerListadoTemperaturaSensorDia($DatoBuscar);             
         echo json_encode($response, JSON_PRETTY_PRINT);
      }

      if ($_GET['action']=='getHistorialPorMes')
      {
         $response = $controlador->ObtenerListadoTemperaturaSensorMes($DatoBuscar);             
         echo json_encode($response, JSON_PRETTY_PRINT);
      }
   }
  }
   function response($code=200, $status="", $message="") 
   {
      http_response_code($code);
      if( !empty($status) && !empty($message) ){
        $response = array("status" => $status ,"message"=>$message);  
        echo json_encode($response,JSON_PRETTY_PRINT);    
      }            
   }   
}
