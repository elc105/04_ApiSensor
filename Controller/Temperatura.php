<?php // Clase que representa la estructura en BD para Usuarios

class Temperatura implements JsonSerializable
{
    protected $ID = 0;
    protected $Temperatura;
    protected $humedad;
    protected $FechaRegistro;
    protected $ClienteID;
    protected $_CreatedAt;
    
    public function __construct() {}
        
    
    function getID() {
        return $this->ID;
    }

    function getTemperatura() {
        return $this->Temperatura;
    }

    function getHumedad() {
        return $this->humedad;
    }

    function getFechaRegistro() {
        return $this->FechaRegistro;
    }

    function getClienteId() {
        return $this->ClienteID;
    }
    function getFechaCreacion() {
        return $this->_CreatedAt;
    }

    function setID($temperatura_id) {
        $this->ID = $temperatura_id;
    }
    function setTemperatura($temperatura) {
        $this->Temperatura = $temperatura;
    }
    function setHumedad($humedad) {
        $this->humedad = $humedad;
    }

    function setFechaRegistro($Fecha) {
        $this->FechaRegistro = $Fecha;
    }

    function setFechaCreacion($Fecha) {
        $this->_CreatedAt = $Fecha;
    }

    function setClienteID($cliente_id) {
        $this->ClienteID = $cliente_id;
    }

    
    public function jsonSerialize() // Esto es para poder visualizar bien el objeto Usuario como JSON
    {
        return 
        [
            'id' => $this->getID(),
            'Temperatura'    => $this->getTemperatura(),
            'Humedad'  => $this->getHumedad(),
            'FechaRegistro' => $this->getFechaRegistro(),
            'ClienteID' => $this->getClienteId(),
            '_CreatedAt' => $this->getFechaCreacion()
        ];
    }
}