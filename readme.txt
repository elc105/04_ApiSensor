ejemplo tomado de: https://github.com/JCorreal/API_REST_PHP

1.- crear la base de datos descrita en el archivo: sensor.sql
2.- copiar la carpeta ApiSensor a htdocs de XAMPP
3.- para correr el ejemplo, primero ejecutar apache 
4.- la url para obtener los datos es: 
	http://localhost:{puerto}/apisensor/temperatura
    
    donde {puerto} es el número de puerto destinado a apache: ejemplo:
	-si tuviera ejecutando en el puerto 1942
	
	  http://localhost:1942/apisensor/temperatura

    si el puerto es el por defecto 80 dejar de la siguiente manera:
	 
 	  http://localhost/apisensor/temperatura